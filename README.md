# Some test PHP

1. Download composer from https://getcomposer.org/
2. Run: php composer.phar install

## Acme files and folders

**Acme sources**

* src/Kith/Acme.php
* src/Kith/AcmePathNotFound.php

**Test acme PHP**

_Note: These tests require write access to project folder_

* bin/behat --profile acme_listing
* bin/behat --profile acme_delete

## Jewelry class hierarchy

**Jewelry sources**

 * src/Kith/Washable.php
 * src/Kith/Jewelry.php
 * src/Kith/Ring.php
 * src/Kith/Necklace.php
 * src/Kith/Bracelet.php

 **Test jewelry PHP**

 * bin/behat --profile jewelry_wash

## Start and end transaction

**Transaction source**

 * src/Kith/TransactionThing.php

 **Test transaction PHP**

 _Note: These tests require write access to project folder_

 * bin/behat --profile transaction
