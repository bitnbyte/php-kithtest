@jewelry_wash
Feature: jewelry
  In order to check that each item washes differently
  As a developer
  I need to be able to check the note of each wash

  Scenario Outline: Wash some jewelry
    Given a jewelry "<type>" item
    Then the wash note should contain "<type>"

    Examples:
    | type     |
    | necklace  |
    | ring     |
    | bracelet |
