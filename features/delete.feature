@acme_delete
Feature: delete
  In order to remove an Acme file
  As a developer
  I need to be able to delete it

  Background:
    Given a folder "temp"
    And file "test.txt" in "temp"
    And file "more.txt" in "temp"
    And folder "afolder" in "temp"
    And file "file1.txt" in "temp/afolder"
    And file "file2.txt" in "temp/afolder"

  Scenario: Delete an existing file
    Given an Acme with path "temp/test.txt"
    When I perform a delete
    Then type should be "notfound"

  Scenario: Delete an existing folder
    Given an Acme with path "temp/afolder"
    When I perform a delete
    Then type should be "notfound"

  Scenario: Delete a non-existing file/folder
    Given an Acme with path "temp/bfolder"
    Then delete should throw "Kith\AcmePathNotFound"

  Scenario: Delete the temp folder
    Given an Acme with path "temp"
    When I perform a delete
    Then type should be "notfound"
