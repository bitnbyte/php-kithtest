@acme_listing
Feature: listing
  In order to know the files
  As a developer
  I need to be able to get a listing

  Background:
    Given a folder "temp"
    And file "test.txt" in "temp"
    And file "more.txt" in "temp"

  Scenario: List an existing folder
    Given an Acme with path "temp"
    Then listing should contain "test.txt"
    And listing should contain "more.txt"

  Scenario: List an existing file
    Given an Acme with path "temp/test.txt"
    Then listing should contain "test.txt"

  Scenario: List a non-existant file/folder
    Given an Acme with path "temp/test2.txt"
    Then listing should throw "Kith\AcmePathNotFound"
