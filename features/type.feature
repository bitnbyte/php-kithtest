@acme_type
Feature: type
  In order to know if I am using a file or a folder
  As a developer
  I need to be able to get the type

  Background:
    Given a folder "temp"
    And file "test.txt" in "temp"
    And file "more.txt" in "temp"

  Scenario: Check existing folder type
    Given an Acme with path "temp"
    Then type should be "folder"

  Scenario: Check non-existant file type
    Given an Acme with path "temp/test2.txt"
    Then type should be "notfound"

  Scenario: Check existing file type
    Given an Acme with path "temp/test.txt"
    Then type should be "file"
