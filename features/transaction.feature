@transaction
Feature: transaction
  In order to know if the goods are added
  As a developer
  I need to be able to get the result of the transaction

  Background:
    Given a database "temp.db"
    And a good "chisel"
    And a good "hammer"
    And a good "gloves"

  Scenario: Add an already existing good
    Given a good "chisel"
    Then adding the good should fail

  Scenario: Add a brand new good
    Given a good "boots"
    Then adding the good should succeed
