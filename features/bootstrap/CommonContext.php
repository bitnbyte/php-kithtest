<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Context\Step\Then,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

use Kith\Acme;

/**
 * Features context.
 */
class CommonContext extends BehatContext
{
    public $acme;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

    /**
     * @Given /^an Acme with path "([^"]*)"$/
     */
    public function anAcmeWithPath($path)
    {
      $this->acme = new Acme($path);

      $parmas = array(
        'acme' => & $this->acme
      );
    }

    /**
     * @Given /^a folder "([^"]*)"$/
     */
    public function aFolder($folder)
    {
      if (!file_exists($folder)) {
        mkdir($folder, 0777, true);
      }
    }

    /**
     * @Given /^file "([^"]*)" in "([^"]*)"$/
     */
    public function fileIn($file, $folder)
    {
      file_put_contents(sprintf(
        "%s/%s", $folder, $file
      ), "");
    }

    /**
     * @Given /^folder "([^"]*)" in "([^"]*)"$/
     */
    public function folderIn($folder, $parent)
    {
      $newFolder = sprintf(
        "%s/%s", $parent, $folder
      );
      return new Then("a folder \"{$newFolder}\"");
    }

    public function __destruct()
    {
      if ($this->acme) {
        try {
          $this->acme->delete(true);
        }
        catch (Exception $e) {
          ;
        }
      }
    }
}
