<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Features context.
 */
class ListingContext extends BehatContext
{
    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
        $this->useContext('common', new CommonContext($parameters));
    }

    /**
     * @Then /^listing should contain "([^"]*)"$/
     */
    public function theListingShouldContain($file)
    {
      $listing = $this->getSubcontext('common')->acme->listing();

      if (!in_array($file, $listing)) {
        throw new Exception(sprintf(
          'But the listing does not contain "%s"', $file
        ));
      }
        // throw new PendingException();
    }

    /**
     * @Then /^listing should throw "([^"]*)"$/
     */
    public function listingShouldThrow($exception)
    {
      $eClass = '';

      try {
        $this->getSubcontext('common')->acme->listing();
      }
      catch (Exception $e) {
        $eClass = get_class($e);
      }

      if ($eClass != $exception) {
        throw new Exception(sprintf(
          'But the exception thrown is "%s"', $eClass
        ));
      }
      else if (!$eClass) {
        throw new Exception('But no exception was thrown.');
      }
    }
}
