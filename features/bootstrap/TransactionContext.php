<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Context\Step\Then,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

use Kith\TransactionThing;

/**
 * Features context.
 */
class TransactionContext extends BehatContext
{
    public $thing;

    private $addResult;
    private $file;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

    /** @BeforeFeature */
    public static function prepareForTheFeature()
    {
      // print("\n <<< preparing for the feature >>> \n");
    }


    /**
     * @Given /^a database "([^"]*)"$/
     */
    public function aDatabase($file)
    {
        $this->file = $file;
        $this->thing = new TransactionThing($file);
    }

    /**
     * @Given /^a good "([^"]*)"$/
     */
    public function aGood($name)
    {
        $this->addResult = $this->thing->addGoods([
          ['name' => $name, 'data' => (object)[]]
        ]);
    }

    /**
     * @Then /^adding the good should fail$/
     */
    public function addingTheGoodShouldFail()
    {
      if (false !== $this->addResult) {
        throw new Exception(
          'But it succeded'
        );
      };
    }

    /**
     * @Then /^adding the good should succeed$/
     */
    public function addingTheGoodShouldSucceed()
    {
        if (true !== $this->addResult) {
          throw new Exception(
            'But it failed'
          );
        };
    }
}
