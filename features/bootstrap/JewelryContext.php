<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Context\Step\Then,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

use Kith\Necklace,
    Kith\Ring,
    Kith\Bracelet;

/**
 * Features context.
 */
class JewelryContext extends BehatContext
{
    private $item;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

    /**
     * @Given /^a jewelry "([^"]*)" item$/
     */
    public function aJewelryItem($type)
    {
      $cName = "Kith\\" . ucfirst($type);
      $this->item = new $cName;
    }

    /**
     * @Given /^the wash note should contain "([^"]*)"$/
     */
    public function theWashNoteShouldContain($type)
    {
      ob_start();
      $this->item->wash();
      $printed = ob_get_clean();;

      if (false === strpos($printed, $type)) {
        throw new Exception(
          'But it does not...'
        );
      }
    }

}
