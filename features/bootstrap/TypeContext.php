<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

use Kith\AcmePathNotFound;

/**
 * Features context.
 */
class TypeContext extends BehatContext
{
    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $this->useContext('common', new CommonContext($parameters));
    }

    /**
     * @Then /^type should be "([^"]*)"$/
     */
    public function typeShouldBe($type)
    {
      $actualType = $this->getSubcontext('common')->acme->type();

      if ($type !== $actualType) {
        throw new Exception(sprintf(
          'But the actual type is "%s"', $actualType
        ));
      }
    }
}
