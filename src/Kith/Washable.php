<?php

namespace Kith;

interface Washable {
  public function wash();
}
