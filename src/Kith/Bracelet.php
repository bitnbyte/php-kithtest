<?php

namespace Kith;

class Bracelet extends Jewelry
{
  public function __construct() {
    parent::__construct(2.5, 0.20, 9.00, 'white');
  }

  public function wash() {
    printf("Scrubbing a %s bracelet.\n", $this->color);
  }
}
