<?php

namespace Kith;

class Necklace extends Jewelry
{
  public function __construct() {
    parent::__construct(7.0, 0.40, 12.25, 'green');
  }

  public function wash() {
    printf("Polishing a %s necklace.\n", $this->color);
  }
}
