<?php

namespace Kith;

abstract class Jewelry implements Washable
{
  protected $size;
  protected $weight;
  protected $price;
  protected $color;

  protected function __construct(
    $size, $weight, $price, $color)
  {
    $this->size = $size;
    $this->weight = $weight;
    $this->price = $price;
    $this->color = $color;
  }
}
