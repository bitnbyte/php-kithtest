<?php

namespace Kith;

class TransactionThing
{
  private $pdo;

  public function __construct($dbFile)
  {
    $createTable = !file_exists($dbFile);

    $this->pdo = new \PDO(
      "sqlite:{$dbFile}"
    );

    if ($createTable) {
      $result = $this->pdo->exec(
         'CREATE TABLE goods ('.
         '  goods_id INTEGER PRIMARY KEY AUTOINCREMENT'.
         ', name TEXT NOT NULL UNIQUE'.
         ', data TEXT NOT NULL DEFAULT "{}"'.
         ');'.
         'CREATE UNIQUE INDEX goodsname ON goods(name);'
      );
    }
  }

  public function addGoods($goods)
  {
    $st = $this->pdo->prepare(
      'INSERT INTO goods (name, data) '.
      'VALUES (:name, :data);'
    );

    $this->pdo->beginTransaction();

    $totalGoods = count($goods);
    $goodsAdded = 0;

    foreach ($goods as $good) {
      $affectedRows = $st->execute([
        ':name' => $good['name'],
        ':data' => json_encode($good['data'])
      ]);
      $goodsAdded += $affectedRows;
    }

    $goodsRemaining = $totalGoods - $goodsAdded;

    if ($goodsRemaining) {
      $this->pdo->rollBack();
      return false;
    } else {
      $this->pdo->commit();
      return true;
    }
  }

  public function getGoods()
  {
    $q = 'SELECT * FROM goods;';

    $goods = $this->pdo
          ->query($q)->fetchAll(\PDO::FETCH_OBJ);

    return $goods;
  }

  public function __destruct()
  {
    $r = $this->pdo->query('PRAGMA database_list;')->fetchObject();
    $this->pdo = null;
    @unlink($r->file);
  }

}
