<?php

namespace Kith;

class Ring extends Jewelry
{
  public function __construct() {
    parent::__construct(0.5, 0.04, 50.50, 'silver');
  }

  public function wash() {
    printf("Cleaning a %s ring.\n", $this->color);
  }
}
