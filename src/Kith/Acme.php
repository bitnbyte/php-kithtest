<?php

namespace Kith;

class Acme
{
  const NOTFOUND = 0;
  const FILE = 1;
  const FOLDER = 2;

  public $path = null;
  private $type = 0;

  public function __construct($path) {
    $this->path = $path;
    $this->refresh();
  }

  public function type() {
    switch ($this->type) {
      case self::NOTFOUND:
        return "notfound";
      case self::FOLDER:
        return "folder";
      case self::FILE:
        return "file";
    }
  }

  public function listing() {
    if ($this->type === self::NOTFOUND) {
      // return [];
      throw new AcmePathNotFound;
    }
    else if ($this->type === self::FILE) {
      return [basename($this->path)];
    }
    else {
      $listing = [];
      $d = dir($this->path);
      while (false !== ($file = $d->read())) {
        if ($file !== '.'
            && $file !== '..') {
              $listing[] = $file;
        }
      }
      closedir($d->handle);
      return $listing;
    }
  }

  public function delete() {
    if ($this->type === self::FILE) {
      @unlink($this->path);
    }
    else if ($this->type === self::FOLDER) {
      $listing = $this->listing();
      while ($file = array_pop($listing)) {
        if ($file !== '.'
            && $file !== '..') {
              $acme = new Acme($this->path . '/' . $file);
              $acme->delete();
        }
      }
      rmdir($this->path);
    }
    else {
      throw new AcmePathNotFound;
    }
    $this->refresh();
  }

  private function refresh() {
    if (file_exists($this->path)) {
      if (is_dir($this->path)) {
        $this->type = self::FOLDER;
      }
      else {
        $this->type = self::FILE;
      }
    }
    else {
      $this->type = self::NOTFOUND;
    }
  }

}
